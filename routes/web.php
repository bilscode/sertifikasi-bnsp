<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', App\Http\Livewire\Home\Index::class)->name('home');
Route::get('/place/{slug}', App\Http\Livewire\Place\Index::class)->name('place');
Route::get('/booking', App\Http\Livewire\Booking\Index::class)->name('booking');
Route::get('/ticket/check', App\Http\Livewire\Ticket\Check::class)->name('ticket.check');
Route::get('/ticket/data', App\Http\Livewire\Ticket\data::class)->name('ticket.data');
Route::get('/ticket/detail/{code}', App\Http\Livewire\Ticket\Detail::class)->name('ticket.detail');
