@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-350px min-h-lg-500px px-9">
    <div class="text-center mb-5 mb-lg-10 py-10 py-lg-20">
        <h1 class="text-white lh-base fw-bolder fs-2x fs-lg-3x mb-15">Wonderful Bekasi</h1>
        <a href="{{ route('booking') }}" class="btn btn-primary">Pesan Tiket</a>
    </div>
</div>
@endsection
<div class="d-flex flex-column flex-root">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="text-center mb-17">
                <h3 class="fs-2hx text-dark mb-5" id="how-it-works" data-kt-scroll-offset="{default: 100, lg: 150}">Cara Pesan Tiket</h3>
                <div class="fs-5 text-muted fw-bold">Pesan tiket wisata yang akan anda kunjungi</div>
            </div>
            <div class="row w-100 gy-10 mb-md-20">
                <div class="col-md-4 px-5">
                    <div class="text-center mb-10 mb-md-0">
                        <img src="media/illustrations/sigma-1/2.png" class="mh-125px mb-9" alt="" />
                        <div class="d-flex flex-center mb-5">
                            <span class="badge badge-circle badge-light-success fw-bolder p-5 me-3 fs-3">1</span>
                            <div class="fs-5 fs-lg-3 fw-bolder text-dark">Pilih Wisata</div>
                        </div>
                        <div class="fw-bold fs-6 fs-lg-4 text-muted">Pilih wisata yang ingin <br/>anda kunjungi</div>
                    </div>
                </div>
                <div class="col-md-4 px-5">
                    <div class="text-center mb-10 mb-md-0">
                        <img src="media/illustrations/sigma-1/8.png" class="mh-125px mb-9" alt="" />
                        <div class="d-flex flex-center mb-5">
                            <span class="badge badge-circle badge-light-success fw-bolder p-5 me-3 fs-3">2</span>
                            <div class="fs-5 fs-lg-3 fw-bolder text-dark">Isi Data Diri</div>
                        </div>
                        <div class="fw-bold fs-6 fs-lg-4 text-muted">Isi data diri sesuai kartu <br/>identitas anda</div>
                    </div>
                </div>
                <div class="col-md-4 px-5">
                    <div class="text-center mb-10 mb-md-0">
                        <img src="media/illustrations/sigma-1/12.png" class="mh-125px mb-9" alt="" />
                        <div class="d-flex flex-center mb-5">
                            <span class="badge badge-circle badge-light-success fw-bolder p-5 me-3 fs-3">3</span>
                            <div class="fs-5 fs-lg-3 fw-bolder text-dark">Cek Tiket</div>
                        </div>
                        <div class="fw-bold fs-6 fs-lg-4 text-muted">Cek pesanan tiket anda dan kunjungi <br/> tempat wisata</div>
                    </div>
                </div>
            </div>
            <div class="tns tns-default" wire:ignore>
                <div data-tns="true" data-tns-loop="true" data-tns-swipe-angle="false" data-tns-speed="2000" data-tns-autoplay="true" data-tns-autoplay-timeout="18000" data-tns-controls="true" data-tns-nav="false" data-tns-items="1" data-tns-center="false" data-tns-dots="false" data-tns-prev-button="#kt_team_slider_prev1" data-tns-next-button="#kt_team_slider_next1">
                    @foreach ($places as $slug_place => $thumbnail)
                    <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10 position-relative">
                        <img src="{{ $thumbnail['thumbnail'] }}" class="card-rounded shadow w-100" alt="{{ $slug_place }}" />
                        <p class="fw-bolder position-absolute top-50 fs-3 fs-lg-1 text-light text-center w-100 start-0">{{ $thumbnail['name'] }}</p>
                    </div>
                    @endforeach
                </div>
                <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_prev1">
                    <span class="svg-icon svg-icon-3x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M11.2657 11.4343L15.45 7.25C15.8642 6.83579 15.8642 6.16421 15.45 5.75C15.0358 5.33579 14.3642 5.33579 13.95 5.75L8.40712 11.2929C8.01659 11.6834 8.01659 12.3166 8.40712 12.7071L13.95 18.25C14.3642 18.6642 15.0358 18.6642 15.45 18.25C15.8642 17.8358 15.8642 17.1642 15.45 16.75L11.2657 12.5657C10.9533 12.2533 10.9533 11.7467 11.2657 11.4343Z" fill="currentColor" />
                        </svg>
                    </span>
                </button>
                <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_next1">
                    <span class="svg-icon svg-icon-3x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M12.6343 12.5657L8.45001 16.75C8.0358 17.1642 8.0358 17.8358 8.45001 18.25C8.86423 18.6642 9.5358 18.6642 9.95001 18.25L15.4929 12.7071C15.8834 12.3166 15.8834 11.6834 15.4929 11.2929L9.95001 5.75C9.5358 5.33579 8.86423 5.33579 8.45001 5.75C8.0358 6.16421 8.0358 6.83579 8.45001 7.25L12.6343 11.4343C12.9467 11.7467 12.9467 12.2533 12.6343 12.5657Z" fill="currentColor" />
                        </svg>
                    </span>
                </button>
            </div>
        </div>
    </div>

    <div class="mt-sm-n10">
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z" fill="currentColor"></path>
            </svg>
        </div>
        <div class="pb-15 pt-18 landing-dark-bg" wire:ignore>
            <div class="container">
                <div class="text-center mt-15 mb-18" id="achievements" data-kt-scroll-offset="{default: 100, lg: 150}">
                    <h3 class="fs-2hx text-white fw-bolder mb-5">Statistik Pemesanan</h3>
                    <div class="fs-5 text-gray-700 fw-bold">Kami telah melayani banyak pengunjung <br/>wisata dengan respon yang baik</div>
                </div>
                <div class="d-flex flex-center">
                    <div class="d-flex flex-wrap flex-center justify-content-lg-between mb-15 mx-auto w-xl-900px">
                        <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain" style="background-image: url({{ asset('media/svg/misc/octagon.svg') }})">
                            <span class="svg-icon svg-icon-2tx svg-icon-white mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                    <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                    <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                    <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                </svg>
                            </span>
                            <div class="mb-0">
                                <div class="fs-lg-2hx fs-2x fw-bolder text-white d-flex flex-center">
                                    <div data-kt-countup="true" data-kt-countup-value="{{ count($places) }}">0</div>
                                </div>
                                <span class="text-gray-600 fw-bold fs-5 lh-0">Lokasi Wisata</span>
                            </div>
                        </div>
                        <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain" style="background-image: url({{ asset('media/svg/misc/octagon.svg') }})">
                            <span class="svg-icon svg-icon-2tx svg-icon-white mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M13 10.9128V3.01281C13 2.41281 13.5 1.91281 14.1 2.01281C16.1 2.21281 17.9 3.11284 19.3 4.61284C20.7 6.01284 21.6 7.91285 21.9 9.81285C22 10.4129 21.5 10.9128 20.9 10.9128H13Z" fill="currentColor" />
                                    <path opacity="0.3" d="M13 12.9128V20.8129C13 21.4129 13.5 21.9129 14.1 21.8129C16.1 21.6129 17.9 20.7128 19.3 19.2128C20.7 17.8128 21.6 15.9128 21.9 14.0128C22 13.4128 21.5 12.9128 20.9 12.9128H13Z" fill="currentColor" />
                                    <path opacity="0.3" d="M11 19.8129C11 20.4129 10.5 20.9129 9.89999 20.8129C5.49999 20.2129 2 16.5128 2 11.9128C2 7.31283 5.39999 3.51281 9.89999 3.01281C10.5 2.91281 11 3.41281 11 4.01281V19.8129Z" fill="currentColor" />
                                </svg>
                            </span>
                            <div class="mb-0">
                                <div class="fs-lg-2hx fs-2x fw-bolder text-white d-flex flex-center">
                                    <div data-kt-countup="true" data-kt-countup-value="1252" data-kt-countup-suffix="+">0</div>
                                </div>
                                <span class="text-gray-600 fw-bold fs-5 lh-0">Pengunjung Dewasa</span>
                            </div>
                        </div>
                        <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain" style="background-image: url({{ asset('media/svg/misc/octagon.svg') }})">
                            <span class="svg-icon svg-icon-2tx svg-icon-white mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M21 10H13V11C13 11.6 12.6 12 12 12C11.4 12 11 11.6 11 11V10H3C2.4 10 2 10.4 2 11V13H22V11C22 10.4 21.6 10 21 10Z" fill="currentColor" />
                                    <path opacity="0.3" d="M12 12C11.4 12 11 11.6 11 11V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V11C13 11.6 12.6 12 12 12Z" fill="currentColor" />
                                    <path opacity="0.3" d="M18.1 21H5.9C5.4 21 4.9 20.6 4.8 20.1L3 13H21L19.2 20.1C19.1 20.6 18.6 21 18.1 21ZM13 18V15C13 14.4 12.6 14 12 14C11.4 14 11 14.4 11 15V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18ZM17 18V15C17 14.4 16.6 14 16 14C15.4 14 15 14.4 15 15V18C15 18.6 15.4 19 16 19C16.6 19 17 18.6 17 18ZM9 18V15C9 14.4 8.6 14 8 14C7.4 14 7 14.4 7 15V18C7 18.6 7.4 19 8 19C8.6 19 9 18.6 9 18Z" fill="currentColor" />
                                </svg>
                            </span>
                            <div class="mb-0">
                                <div class="fs-lg-2hx fs-2x fw-bolder text-white d-flex flex-center">
                                    <div data-kt-countup="true" data-kt-countup-value="752" data-kt-countup-suffix="+">0</div>
                                </div>
                                <span class="text-gray-600 fw-bold fs-5 lh-0">Pengunjung Anak-anak</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>

    <div class="mt-20 mb-n20 position-relative z-index-2">
        <div class="container">
            <div class="text-center mb-12">
                <h3 class="fs-2hx text-dark mb-5" id="team" data-kt-scroll-offset="{default: 100, lg: 150}">Lokasi Wisata</h3>
            </div>
            <div class="row g-5 mb-10">
                @foreach ($places as $slug => $place)
                <div class="col-md-6" wire:key="place-list-{{ $slug }}">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="h-300px w-100 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-center rounded mb-5" style="background: url({{ $place['thumbnail'] }})"></div>
                            <a href="{{ route('place', $slug) }}" class="text-black text-hover-primary fw-bolder fs-4 fs-lg-2">{{ $place['name'] }}</a>
                            <p class="text-muted my-5"><i class="fas fa-map me-2"></i>{{ $place['address'] }}</p>
                            <p>{!! Str::words($place['description'], 50, '...') !!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="d-flex flex-stack flex-wrap flex-md-nowrap card-rounded shadow p-8 p-lg-12 mb-n5 mb-lg-n13" style="background: linear-gradient(90deg, #20AA3E 0%, #03A588 100%);">
                <div class="my-2 me-5">
                    <div class="fs-1 fs-lg-2qx fw-bolder text-white">Pesan Tiket Wisata,
                        <span class="fw-normal">Sekarang!</span>
                    </div>
                </div>
                <a href="{{ route('booking') }}" class="btn btn-lg btn-outline border-2 btn-outline-white flex-shrink-0 my-2">Pesan Tiket</a>
            </div>
        </div>
    </div>
</div>
