@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-100px">
    &nbsp;
</div>
@endsection

<div class="d-flex flex-column flex-root mt-n20 z-index-2">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="card">
                <div class="card-body p-lg-20">
                    <form wire:submit.prevent="checkTicket">
                        <label class="form-label">Masukan Kode Tiket</label>
                        <input type="text" class="form-control" placeholder="contoh : 111122223333" wire:model.defer="code" />
                        @error('code')
                        <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                        @enderror

                        <button class="btn btn-primary btn-lg mt-5" type="submit" wire:click="checkTicket" wire:target="checkTicket" wire:loading.class="spinner spinner-light spinner-right" wire:offline.attr="disabled">
                            <div wire:loading.remove wire:target="checkTicket">
                                Cek
                            </div>
                            <div wire:loading wire:target="checkTicket">
                                Tunggu Sebentar...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </div>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
