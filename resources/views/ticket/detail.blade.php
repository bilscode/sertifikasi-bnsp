@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-100px">
    &nbsp;
</div>
@endsection

<div class="d-flex flex-column flex-root mt-n20 z-index-2">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="row g-5">
                <div class="col-md-8">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h3>Kode Tiket : #{{ $ticket->code }}</h3>
                            <div class="separator separator-dashed my-9"></div>
                            <p>Status Pemesanan : {{ Str::title(Str::replace('_', ' ', $ticket->status)) }}</p>
                            <p>Nama Pemesan : {{ $ticket->name }}</p>
                            <p>Nomor Identitas : {{ $ticket->identity_number }}</p>
                            <p>Nomor HP : {{ $ticket->phone_number }}</p>
                            <p>Tempat Wisata : {{ $place['name'] }}</p>
                            <p>Tanggal Kunjungan : {{ \Carbon\Carbon::parse($ticket->visit_date)->format('d-m-Y') }}</p>
                            <p>Pengunjung Dewasa : {{ $ticket->adult_visitor }}</p>
                            <p>Pengunjung Anak-anak : {{ $ticket->child_visitor }}</p>
                            <div class="separator separator-dashed my-9"></div>
                            <div class="d-flex justify-content-between">
                                <p class="fs-5 mb-0">Harga Tiket</p>
                                <p class="fs-5 text-primary fw-bolder mb-0">{{ Helpers::formatCurrency($ticket->ticket_price) }}</p>
                            </div>
                            <div class="my-2"></div>
                            <div class="d-flex justify-content-between">
                                <p class="fs-5 mb-0">Diskon</p>
                                <p class="fs-5 text-primary fw-bolder mb-0">({{ Helpers::formatCurrency($ticket->ticket_price - $ticket->total_price) }})</p>
                            </div>
                            <div class="my-2"></div>
                            <div class="d-flex justify-content-between">
                                <p class="fs-5 mb-0">Total Bayar</p>
                                <p class="fs-5 text-primary fw-bolder mb-0">{{ Helpers::formatCurrency($ticket->total_price) }}</p>
                            </div>
                            @if ($ticket->status == 'MENUNGGU_PEMBAYARAN')
                            <div class="separator separator-dashed my-9"></div>
                            <button class="btn btn-danger w-100" wire:click="cancel({{ $ticket->id }})">Batalkan Pemesanan</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h3>{{ $ticket->status == 'MENUNGGU_PEMBAYARAN' ? 'Upload ' : null }}Bukti Pembayaran</h3>
                            <div class="separator separator-dashed my-9"></div>
                            @if ($ticket->status == 'MENUNGGU_PEMBAYARAN')
                            <form wire:submit.prevent="uploadPayment({{ $ticket->id }})">
                                <div class="mb-10">
                                    <input type="file" class="form-control" wire:model.defer="payment"/>
                                    @error('payment')
                                        <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="separator separator-dashed my-9"></div>
                                <button class="btn btn-primary btn-lg w-100" type="button" wire:click="uploadPayment({{ $ticket->id }})" wire:target="uploadPayment({{ $ticket->id }})" wire:loading.class="spinner spinner-light spinner-right" wire:offline.attr="disabled">
                                    <div wire:loading.remove wire:target="uploadPayment({{ $ticket->id }})">
                                        Upload
                                    </div>
                                    <div wire:loading wire:target="uploadPayment({{ $ticket->id }})">
                                        Tunggu Sebentar...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </div>
                                </button>
                            </form>
                            @else
                            <img src="{{ asset('storage/payments') }}/{{ $ticket->payment }}" alt="{{ $ticket->code }}" class="w-100 rounded"/>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
