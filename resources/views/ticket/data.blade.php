@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-100px">
    &nbsp;
</div>
@endsection

<div class="d-flex flex-column flex-root mt-n20 z-index-2">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="card">
                <div class="card-body shadow-sm">
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed gy-5 gs-5 fs-6 border table-rounded">
                            <thead>
                                <tr class="text-muted fw-bolder fs-6 text-uppercase gs-0">
                                    <th class="text-nowrap">Kode Pemesanan</th>
                                    <th class="text-nowrap">Nama Pemesan</th>
                                    <th class="text-nowrap">Tempat Wisata</th>
                                    <th class="text-nowrap">Jumlah Pengunjung</th>
                                    <th class="text-nowrap">Status</th>
                                    <th class="text-nowrap">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="fs-6 fw-bold text-dark">
                                @foreach ($tickets as $ticket)
                                <tr>
                                    <td>{{ $ticket->code }}</td>
                                    <td>{{ $ticket->name }}</td>
                                    <td>{{ Constant::places()[$ticket->place]['name'] }}</td>
                                    <td>{{ $ticket->adult_visitor+$ticket->child_visitor }}</td>
                                    <td>{{ Str::title(Str::replace('_', ' ', $ticket->status)) }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-icon btn-sm" type="button" wire:click="deleteTicket({{ $ticket->id }})"><i class="fas fa-trash"></i></button>
                                        <a href="{{ route('ticket.detail', $ticket->code) }}" class="btn btn-primary btn-icon btn-sm"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $tickets->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
