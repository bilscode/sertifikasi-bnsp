@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-100px">
    &nbsp;
</div>
@endsection
<div class="d-flex flex-column flex-root mt-n20 z-index-2">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="row g-5">
                <div class="col-md-8 d-flex flex-column">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h3 class="text-dark mb-7">Formulir Pemesanan</h3>
                            <div class="separator separator-dashed mb-9"></div>
                            <div class="mb-10">
                                <label class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" placeholder="contoh : Billisany Akhyar" wire:model="name" />
                                @error('name')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Nomor Identitas (NIK)</label>
                                <input type="number" class="form-control" placeholder="contoh : 1111222233334444" wire:model="identity_number"/>
                                @error('identity_number')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Nomor HP</label>
                                <input type="number" class="form-control" placeholder="contoh : 628123456789" wire:model="phone_number"/>
                                @error('phone_number')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Tempat Wisata</label>
                                <select class="form-select" wire:model="place">
                                    <option value="">Pilih</option>
                                    @foreach ($places as $place_key => $place_data)
                                    <option value="{{ $place_key }}">{{ $place_data['name'] }}</option>
                                    @endforeach
                                </select>
                                @error('place')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Tanggal Kunjungan</label>
                                <input type="date" class="form-control" wire:model="visit_date"/>
                                @error('visit_date')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Pengunjung Dewasa</label>
                                <input type="number" class="form-control" placeholder="contoh : 2" wire:model="adult_visitor"/>
                                @error('adult_visitor')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-10">
                                <label class="form-label">Pengunjung Anak-anak</label>
                                <small class="fst-italic text-muted">(usia dibawah 12 tahun)</small>
                                <input type="number" class="form-control" placeholder="contoh : 3" wire:model="child_visitor"/>
                                @error('child_visitor')
                                    <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex flex-column">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h3 class="text-dark mb-7">Pembayaran</h3>
                            <div class="separator separator-dashed mb-9"></div>
                            <div class="d-flex justify-content-between">
                                <p class="fs-5 mb-0">Harga Tiket</p>
                                <p class="fs-5 text-primary fw-bolder mb-0">{{ Helpers::formatCurrency($ticket_price) }}</p>
                            </div>
                            <div class="separator separator-dashed my-5"></div>
                            <div class="d-flex justify-content-between mb-5">
                                <p class="fs-5 mb-0">Total Bayar</p>
                                <p class="fs-5 text-primary fw-bolder mb-0">{{ Helpers::formatCurrency($total_price) }}</p>
                            </div>
                            <div class="mb-5">
                                <div class="form-check form-check-custom form-check-solid mb-2">
                                    <input class="form-check-input" type="checkbox" id="agreement" wire:model="agreement" value="true"/>
                                    <label class="form-check-label" for="agreement">
                                        Saya telah  setuju dengan syarat dan ketentuan.
                                    </label>
                                </div>
                                @error('agreement')
                                <p class="mb-0 text-danger fw-bolder">{{ $message }}</p>
                                @enderror
                            </div>
                            <button class="btn btn-primary btn-lg w-100" type="button" wire:click="booking" wire:target="booking" wire:loading.class="spinner spinner-light spinner-right" wire:offline.attr="disabled">
                                <div wire:loading.remove wire:target="booking">
                                    Pesan Sekarang
                                </div>
                                <div wire:loading wire:target="booking">
                                    Tunggu Sebentar...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
