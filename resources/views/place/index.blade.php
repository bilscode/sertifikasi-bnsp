@section('header')
<div class="d-flex flex-column flex-center w-100 min-h-100px">
    &nbsp;
</div>
@endsection
<div class="d-flex flex-column flex-root mt-n20 z-index-2">
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="card shadow-sm">
                <div class="card-body p-lg-20">
                    <h3 class="text-dark mb-7">{{ $place['name'] }}</h3>
                    <div class="separator separator-dashed mb-9"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="h-100 d-flex flex-column pe-lg-6 mb-lg-0 mb-10">
                                <img src="{{ $place['thumbnail'] }}" alt="{{ $place['name'] }}" class="w-100 rounded mb-3"/>
                                <div class="fw-bold fs-5 text-gray-600 text-dark mb-5">{{ $place['description'] }}</div>
                                <h3 class="text-primary">Harga Tiket : {{ Helpers::formatCurrency($place['price']) }}</h3>
                                <div class="separator separator-dashed my-5"></div>
                                <p class="fw-bolder">Video Preview</p>
                                <iframe class="w-100 min-h-200px min-h-lg-325px rounded" src="{{ $place['video'] }}" title="{{ $place['name'] }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-md-6 d-flex flex-column">
                            <div class="separator separator-dashed mb-5 d-block d-md-none"></div>
                            <p class="fw-bolder">Alamat</p>
                            <p class="text-muted"><i class="fas fa-map me-2"></i>{{ $place['address'] }}</p>
                            <iframe src="{{ $place['map'] }}" class="w-100 min-h-200px min-h-lg-300px rounded" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            <div class="separator separator-dashed my-5"></div>
                            <p class="fw-bolder mb-0">Grafik Pengunjung</p>
                            <p class="text-muted"><small>7 Hari terakhir</small></p>
                            <div id="chart_visitor" style="height: 350px;" class="border border-primary border-1 rounded" wire:ignore></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    var element = document.getElementById('chart_visitor');

    var height = parseInt(KTUtil.css(element, 'height'));
    var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
    var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
    var baseColor = KTUtil.getCssVariableValue('--bs-info');
    var lightColor = KTUtil.getCssVariableValue('--bs-light-info');

    var options = {
        series: [{
            name: 'Pengunjung',
            data: @json($place['visitor'])
        }],
        chart: {
            fontFamily: 'inherit',
            type: 'area',
            height: height,
            toolbar: {
                show: false
            }
        },
        plotOptions: {

        },
        legend: {
            show: false
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            type: 'solid',
            opacity: 1
        },
        stroke: {
            curve: 'smooth',
            show: true,
            width: 3,
            colors: [baseColor]
        },
        xaxis: {
            categories: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
            axisBorder: {
                show: false,
            },
            axisTicks: {
                show: false
            },
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            },
            crosshairs: {
                position: 'front',
                stroke: {
                    color: baseColor,
                    width: 1,
                    dashArray: 3
                }
            },
            tooltip: {
                enabled: true,
                formatter: undefined,
                offsetY: 0,
                style: {
                    fontSize: '12px'
                }
            }
        },
        yaxis: {
            labels: {
                style: {
                    colors: labelColor,
                    fontSize: '12px'
                }
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0
                }
            },
            active: {
                allowMultipleDataPointsSelection: false,
                filter: {
                    type: 'none',
                    value: 0
                }
            }
        },
        tooltip: {
            style: {
                fontSize: '12px'
            },
        },
        colors: [lightColor],
        grid: {
            borderColor: borderColor,
            strokeDashArray: 4,
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        markers: {
            strokeColor: baseColor,
            strokeWidth: 3
        }
    };

    var chart = new ApexCharts(element, options);
    chart.render();
</script>
@endpush
