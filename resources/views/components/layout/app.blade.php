<!DOCTYPE html>
<html prefix="og: https://ogp.me/ns#" lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <x-layout.seo title="{{ $title }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

	<livewire:styles />

    <style type="text/css" media="print">
        @page {
            size: auto;
            margin: 2rem;
        }
        * {
            -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
            color-adjust: exact !important;                 /*Firefox*/
        }
    </style>

    @stack('styles')
    <style>
        .tooltip {
            z-index: 99999999999;
        }
        .box-package {
            transition: all 0.2s ease-in;
            box-shadow: 0 0.1rem 1rem 0.25rem rgb(0 0 0 / 5%) !important;
        }
        .box-package:hover{
            transform: scale(1.02);
            box-shadow: 0 1rem 2rem 1rem rgb(0 0 0 / 10%) !important;
            z-index:9999;
        }
    </style>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
</head>

<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200" class="bg-white position-relative">

    <div class="{{ $class ?? 'mb-0' }}" id="home">
        <div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-center landing-dark-bg" style="background-image: url({{ asset('media/svg/illustrations/landing.svg') }})">
            <div class="landing-header" data-kt-sticky="true" data-kt-sticky-name="landing-header" data-kt-sticky-offset="{default: '200px', lg: '300px'}">
                <div class="container">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center flex-equal">
                            <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none" id="kt_landing_menu_toggle">
                                <span class="svg-icon svg-icon-2hx">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
                                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
                                    </svg>
                                </span>
                            </button>
                            <a href="{{ route('home') }}">
                                <img alt="Logo" src="{{ asset('img/logo.png') }}" class="logo-default h-25px h-lg-30px" />
                                <img alt="Logo" src="{{ asset('img/logo.png') }}" class="logo-sticky h-20px h-lg-25px" />
                            </a>
                        </div>
                        <div class="d-lg-block" id="kt_header_nav_wrapper">
                            <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="200px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                                <div class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-gray-500 menu-state-title-primary nav nav-flush fs-5 fw-bold" id="kt_landing_menu">
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="{{ route('home') }}" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Beranda</a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="{{ route('booking') }}" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Pemesanan Tiket</a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6" href="{{ route('ticket.data') }}" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Data Tiket</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-equal text-end ms-1">
                            <a href="{{ route('ticket.check') }}" class="btn btn-success">Cek Tiket</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('header')
        </div>

        <div class="landing-curve landing-dark-color mb-10 mb-lg-20">
            <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>

    {{ $slot }}

    <div class="mb-0">
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z" fill="currentColor"></path>
            </svg>
        </div>
        <div class="landing-dark-bg pt-20">
            <div class="container">
                <div class="row py-10 py-lg-20">
                    <div class="col-lg-6 pe-lg-16 mb-10 mb-lg-0">
                        <div class="rounded landing-dark-border p-9 mb-10">
                            <h2 class="text-white">Dibuat Oleh Billisany Akhyar</h2>
                            <span class="fw-normal fs-4 text-gray-700">Email
                            <a href="https://bilscode.com/contact" class="text-white opacity-50 text-hover-primary">billisanyakhyar24@gmail.com</a></span>
                        </div>
                    </div>
                    <div class="col-lg-6 ps-lg-16">
                        <div class="d-flex justify-content-center">
                            <div class="d-flex fw-bold flex-column ms-lg-20">
                                <h4 class="fw-bolder text-gray-400 mb-6">Sosial Media</h4>
                                <a target="_blank" href="https://gitlab.com/bilscode" class="mb-6">
                                    <img src="{{ asset('media/svg/brand-logos/gitlab.svg') }}" class="h-20px me-2" alt="sosmed" />
                                    <span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Gitlab</span>
                                </a>
                                <a target="_blank" href="https://www.linkedin.com/in/billisany-akhyar-86009a152/" class="mb-6">
                                    <img src="{{ asset('media/svg/brand-logos/linkedin-2.svg') }}" class="h-20px me-2" alt="sosmed" />
                                    <span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Linkedin</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="landing-dark-separator"></div>
            <div class="container">
                <div class="d-flex align-items-center justify-content-center py-7 py-lg-10">
                    <a href="{{ route('home') }}">
                        <img alt="Logo" src="{{ asset('img/logo.png') }}" class="h-15px h-md-20px" />
                    </a>
                    <a target="_blank" class="mx-5 fs-6 fw-bold text-gray-600 pt-1" href="https://bilscode.com">© 2022 Bilscode.</a>
                </div>
            </div>
        </div>
    </div>

    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        {!! Helpers::getSvgIcon('arrows/arr066.svg') !!}
    </div>

    <script>var hostUrl = "{{ asset('/') }}";</script>
    <script src="{{ asset('plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('js/scripts.bundle.js') }}"></script>

    <script>
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toastr-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        var Tawk_API=Tawk_API||{};
        var Tawk_LoadStart=new Date();

        window.addEventListener('modal-hide', event => {
            var target = event.detail.modal
            $('#'+target).modal('hide');
        })
        window.addEventListener('modal-show', event => {
            var target = event.detail.modal
            $('#'+target).modal('show');
        })

        function printArea(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>

	<livewire:scripts />

    @stack('script')

</body>

</html>
