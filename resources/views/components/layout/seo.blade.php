<title>{{ $title }} - {{ env('APP_NAME') }}</title>

<meta content="{{ env('APP_NAME') }}  - Pemesanan Tiket" name="description" />
<meta name="keywords" content="pemesanan, tiket, online" />

<meta property="og:locale" content="id_ID" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ $title }} - {{ env('APP_NAME') }}" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:site_name" content="{{ env('APP_NAME') }} | Bilscode" />
<meta property="og:description" content="Pemesanan Tiket" />
<meta property="og:image" content="{{ asset('img/logo.png') }}" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="{{ $title }} - {{ env('APP_NAME') }}" />
<meta name="twitter:description" content="Pemesanan Tiket" />
<meta name="twitter:image" content="{{ asset('img/logo.png') }}" />

<link rel="canonical" href="{{ url('/') }}" />
<link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
