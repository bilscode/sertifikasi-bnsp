# Website Serkom Tema Wisata - Wonderful Bekasi

![Wonderful Bekasi](/public/img/logo.png "Wonderful Bekasi")

## Deskripsi Aplikasi
Website yang berisi deskripsi dan pemesanan Tempat Wisata di daerah Bekasi.
Untuk saat ini hanya terdapat 4 tempat wisata :
- Rumah Pohon Jatiasih
- Waterboom Lippo Cikarang
- Situ Gede Bekasi
- Jababeka Botanical Garden

## Tech Stack
- Laravel 9.9.0
- PHP 8.0
- PostgreSQL 14

## Library Back End
Library diinstal melalui composer
1. Livewire 
2. Str
3. Carbon

## Library Front End
1. Bootstrap 5
2. Alpine.js
3. JQuery
4. Apex Chart

## Struktur Folder dan isinya

### **app** 
Direktori `app` berisi kode inti aplikasi.

### **bootstrap** 
Direktori `bootstrap` berisi file app.php yang mem-bootstrap framework. Direktori ini juga menampung direktori cache yang berisi file yang dihasilkan framework untuk pengoptimalan kinerja seperti file cache route dan service.

### **config** 
Direktori `config` berisi semua file konfigurasi aplikasi.

### **database** 
Direktori `database` berisi migration database, model factory, dan seed. 

### **public** 
Direktori `public` berisi file index.php, yang merupakan titik masuk untuk semua permintaan yang memasuki aplikasi kamu dan mengkonfigurasi pemuatan otomatis. Direktori ini juga menampung aset kamu seperti gambar, Javascript, dan CSS.

### **recources** 
Direktori `resources` berisi tampilan serta aset mentah yang belum dicompile seperti CSS atau Javascript. 

### **routes** 
Direktori `routes` berisi semua definisi route untuk aplikasi.

### **storage** 
Direktori `storage` berisi log, template Blade yang dicompile, session berbasis file, cache file, dan file lain yang dihasilkan oleh framework.

### **tests** 
Direktori `tests` berisi tes otomatis.

### **vendor** 
Direktori `vendor` berisi dependensi Composer.




