<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('phone_number');
            $table->string('identity_number');
            $table->string('place');
            $table->date('visit_date');
            $table->integer('adult_visitor');
            $table->integer('child_visitor');
            $table->bigInteger('ticket_price');
            $table->bigInteger('total_price');
            $table->string('payment')->nullable();
            $table->string('status')->default('MENUNGGU_PEMBAYARAN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
