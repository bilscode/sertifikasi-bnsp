<?php
namespace App\Utilities;

class Constant
{

    public static function places()
    {
        return [
            'rumah-pohon-jatiasih' => [
                'thumbnail' => asset('img/places/rumah-pohon-jatiasih.jpg'),
                'name' => 'Rumah Pohon Jatiasih',
                'description' => "Destinasi wisata Bekasi selanjutnya adalah tujuan wisata yang cocok untuk mengenalkan anak-anak terhadap alam.\nObjek wisata keluarga sekaligus wisata alam di Bekasi ini menyajikan pemandangan alam yang asri.\nDi tempat wisata Bekasi satu ini, Toppers bisa berjalan santai dibawah rindangnya pepohonan, menikmati panorama danau sekaligus memancing. Berbagai rumah pohon juga tersedia di komplek wisata Bekasi ini untuk bersantai bersama keluarga.\nAktivitas lain yang bisa Toppers lakukan di tempat wisata Bekasi ini antara lain outbond hingga mengendarai motor ATV",
                'price' => 50000,
                'address' => 'Jl Raya Parpostel, Jatiasih, Bekasi',
                'video' => 'https://www.youtube.com/embed/qnXcB5L97MI',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.611288146011!2d106.95544131535247!3d-6.31468466355383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69928ac933305d%3A0xedb0263d45c680fe!2sRumah%20Pohon!5e0!3m2!1sid!2sid!4v1650564430854!5m2!1sid!2sid',
                'visitor' => [125, 325, 204, 176, 300, 122, 321],
            ],
            'waterboom-lippo-cikarang' => [
                'thumbnail' => asset('img/places/waterboom-lippo-cikarang.jpeg'),
                'name' => 'Waterboom Lippo Cikarang',
                'description' => "Waterboom bisa jadi salah satu pilihan wisata kota Bekasi bersama keluarga, terutama jika Toppers tengah berlibur dengan anak-anak. Waterboom Lippo Cikarang adalah salah satunya.\nObjek wisata keluarga di Bekasi satu ini memiliki konsep desain layaknya pulau Dewata Bali dengan berbagai dekorasi patung hingga pondok-pondok nyaman untuk bersantai.\nBerbagai wahana favorit anak-anak mulai dari kolam renang, kolam arus, dan berbagai perosotan seru membuat tempat wisata Bekasi satu ini nggak pernah sepi saat hari libur tiba",
                'price' => 50000,
                'address' => 'Jl. Madiun Kav. 115, Lippo Cikarang, Cikarang, Bekasi',
                'video' => 'https://www.youtube.com/embed/qnXcB5L97MI',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.482751312981!2d107.13623141535247!3d-6.331444663714742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e699a57b20db445%3A0xf9931a1b0336f09e!2sWaterBoom%20Lippo%20Cikarang!5e0!3m2!1sid!2sid!4v1650564636868!5m2!1sid!2sid',
                'visitor' => [225, 125, 304, 276, 100, 222, 121],
            ],
            'situ-gede-bekasi' => [
                'thumbnail' => asset('img/places/situ-gede-bekasi.jpg'),
                'name' => 'Situ Gede Bekasi',
                'description' => "Situ Gede adalah salah satu objek wisata danau. Dengan luas mencapai 7 hektar, destinasi wisata Bekasi ini cocok untuk kamu yang tengah mencari ketenangan di tengah-tengah suasana alami.\nDi sini, Toppers bisa memancing atau sekedar mengelilingi danau dengan perahu kecil yang disediakan oleh masyarakat lokal.",
                'price' => 50000,
                'address' => 'Jalan Kampung Bojong Menteng, Rawalumbu, Bojong Menteng, Bekasi',
                'video' => 'https://www.youtube.com/embed/qnXcB5L97MI',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.7657417942505!2d106.97628121535233!3d-6.29448656336055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698d8a24c0aa93%3A0x3ef1d1d44541d510!2sSitu%20Rawa%20Gede%20Wisata%20Perahu%20KPPL%20Bojong%20Menteng!5e0!3m2!1sid!2sid!4v1650564603439!5m2!1sid!2sid',
                'visitor' => [425, 225, 104, 376, 200, 122, 221],
            ],
            'jababeka-botanical-garden' => [
                'thumbnail' => asset('img/places/jababeka-botanical-garden.jpg'),
                'name' => 'Jababeka Botanical Garden',
                'description' => "Hutan kota memang bisa jadi alternatif wisata yang menarik, salah satunya adalah Jababeka Botanical Garden, hutan kota yang memiliki suasana asri.\nObjek wisata alam di Bekasi ini digemari sbeagai destinasi jogging atau sekedar berjalan santai menikmati asrinya pepohonan.\nSuasanannya yang instagramable menjadikan destinasi wisata bekasi ini cukup digemari untuk berburu foto juga, lho.",
                'price' => 50000,
                'address' => 'Jalan Cikarang Baru Raya, Sertajaya, Cikarang Timur, Bekasi',
                'video' => 'https://www.youtube.com/embed/qnXcB5L97MI',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1982.8426880375314!2d107.17619045819906!3d-6.305004132872622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e699b404eacb75d%3A0x3a9d5bd36bef3556!2sJababeka%20Botanical!5e0!3m2!1sid!2sid!4v1650564564109!5m2!1sid!2sid',
                'visitor' => [225, 125, 204, 276, 230, 122, 221],
            ]
        ];
    }

}
