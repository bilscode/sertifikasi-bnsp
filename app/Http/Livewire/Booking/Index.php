<?php

namespace App\Http\Livewire\Booking;

use App\Models\Booking;
use App\Utilities\Constant;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class Index extends Component
{
    public $places;
    public $name, $identity_number, $phone_number, $place, $visit_date, $adult_visitor, $child_visitor;
    public $agreement;
    public $ticket_price, $total_price;
    public $place_selected;

    protected function rules()
    {
        $place_keys = implode(',', collect($this->places)->keys()->toArray());
        return [
            'name' => ['required', 'string', 'max:255'],
            'identity_number' => ['required', 'integer', 'digits:16'],
            'phone_number' => ['required', 'numeric', 'digits_between:8,14'],
            'place' => ['required', 'string', 'in:'.$place_keys],
            'visit_date' => ['required', 'date', 'after:yesterday'],
            'adult_visitor' => ['required', 'integer', 'min:1'],
            'child_visitor' => ['nullable', 'integer', 'min:0'],
            'agreement' => ['required', 'string', 'in:true'],
        ];
    }

    public function mount()
    {
        $this->places = Constant::places();
        $this->adult_visitor = 0;
        $this->child_visitor = 0;
        $this->ticket_price = 0;
        $this->total_price = 0;
    }

    public function render()
    {
        return view('booking.index')->layoutData([
            'title' => 'Pemesanan Tiket',
            'class' => 'mb-n20'
        ]);
    }

    public function updatingAdultVisitor($value)
    {
        $validate['adult_visitor'] = $value;
        Validator::make($validate, [
            'adult_visitor' => ['required', 'integer', 'min:0']
        ])->validate();
        $this->resetErrorBag('adult_visitor');
    }

    public function updatingChildVisitor($value)
    {
        $validate['child_visitor'] = $value;
        Validator::make($validate, [
            'child_visitor' => ['required', 'integer', 'min:0']
        ])->validate();
        $this->resetErrorBag('child_visitor');
    }

    public function updatedAdultVisitor()
    {
        $this->calculate();
    }

    public function updatedChildVisitor()
    {
        $this->calculate();
    }

    public function updatedPlace()
    {
        $this->calculate();
    }

    public function calculate()
    {
        try {
            $places = $this->places;
            $place = $places[$this->place];
            $ticket_price = $place['price']*($this->adult_visitor+$this->child_visitor);
            $adult_price = $this->adult_visitor*$place['price'];
            $child_price = ($this->child_visitor*$place['price'])/2;
            $this->ticket_price = $ticket_price;
            $this->total_price = $adult_price+$child_price;
        } catch (\Throwable $th) {
            $this->ticket_price = 0;
            $this->total_price = 0;
        }
    }

    public function booking()
    {
        $data = $this->validate();
        $code = now()->timestamp.rand(1111, 9999);
        Booking::create([
            'code' => $code,
            'name' => $data['name'],
            'phone_number' => $data['phone_number'],
            'identity_number' => $data['identity_number'],
            'place' => $data['place'],
            'visit_date' => $data['visit_date'],
            'adult_visitor' => $data['adult_visitor'],
            'child_visitor' => $data['child_visitor'],
            'ticket_price' => $this->ticket_price,
            'total_price' => $this->total_price,
        ]);
        return redirect()->route('ticket.detail', $code);
    }

}
