<?php

namespace App\Http\Livewire\Ticket;

use Livewire\Component;

class Check extends Component
{
    public $code;

    protected $rules = [
        'code' => ['required', 'numeric', 'exists:bookings,code']
    ];

    public function render()
    {
        return view('ticket.check')->layoutData([
            'title' => 'Cek Tiket',
            'class' => 'mb-n20'
        ]);
    }

    public function checkTicket()
    {
        $this->validate();
        return redirect()->route('ticket.detail', $this->code);
    }
}
