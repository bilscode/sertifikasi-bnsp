<?php

namespace App\Http\Livewire\Ticket;

use App\Models\Booking;
use App\Utilities\Constant;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Detail extends Component
{
    use WithFileUploads;

    public $ticket, $place, $payment;

    public function mount($code)
    {
        $ticket = Booking::where('code', $code)->firstOrFail();
        $this->ticket = $ticket;
        $this->place = Constant::places()[$ticket->place];
    }

    public function render()
    {
        return view('ticket.detail')->layoutData([
            'title' => "{$this->place['name']} - {$this->ticket->code}",
            'class' => 'mb-n20'
        ]);
    }

    public function cancel(Booking $booking)
    {
        $booking->delete();
        return redirect()->route('home');
    }

    public function uploadPayment(Booking $booking)
    {
        $data = $this->validate([
            'payment' => ['required', 'image', 'mimes:png,jpg', 'max:512']
        ]);
        $payment_name = $booking->code.'.'.$data['payment']->getClientOriginalExtension();
        Storage::putFileAs(
            'public/payments',
            $data['payment'],
            $payment_name
        );
        $booking->update([
            'payment' => $payment_name,
            'status' => 'TERBAYAR'
        ]);
        $this->ticket->payment = $payment_name;
        $this->ticket->status = 'TERBAYAR';
    }

}
