<?php

namespace App\Http\Livewire\Ticket;

use App\Models\Booking;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('ticket.data', [
            'tickets' => Booking::paginate(10)
        ])->layoutData([
            'title' => 'Data Tiket',
            'class' => 'mb-n20'
        ]);
    }

    public function deleteTicket(Booking $booking)
    {
        $booking->delete();
    }
}
