<?php

namespace App\Http\Livewire\Home;

use Livewire\Component;
use App\Utilities\Constant;

class Index extends Component
{

    public $places;

    public function mount()
    {
        $this->places = Constant::places();
    }

    public function render()
    {
        return view('home.index')->layoutData([
            'title' => 'Beranda',
        ]);
    }
}
