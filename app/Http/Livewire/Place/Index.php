<?php

namespace App\Http\Livewire\Place;

use App\Utilities\Constant;
use Livewire\Component;

class Index extends Component
{
    public $place;

    public function mount($slug)
    {
        $place = Constant::places();
        try {
            $this->place = $place[$slug];
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public function render()
    {
        return view('place.index')->layoutData([
            'title' => $this->place['name'],
            'class' => 'mb-n20'
        ]);
    }
}
